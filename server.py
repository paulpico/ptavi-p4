#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
from datetime import datetime, date, time, timedelta


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    SIP Register and save users in .json file
    """
    diccdata = {}

    def json2registered(self):
        """
        Realize if the registered.json exists
        """
        try:
            with open('registered.json', 'r') as jsonfile:
                self.diccdata = json.load(jsonfile)
        except FileNotFoundError:
            """
            In other case, the dictionary is the same
            """
            pass

    def register2json(self):
        """
        Create registered.json file
        """
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.diccdata, jsonfile, indent=4)

    def handle(self):
        """
        The dictionary is created from the registered.json file
        """
        lista = []
        client = {'Address': '', 'Expires': ''}
        IP = self.client_address[0]
        PORT = self.client_address[1]
        ADDRESS = IP + ', ' + str(PORT)
        TIME = '%Y-%m-%d %H:%M:%S'
        for line in self.rfile:
            lista.append(line.decode('utf-8'))
        message = lista[0].split()
        method = message[0].split()[0]
        """It checked that the first term was REGISTER"""
        if method == 'REGISTER':
            user = message[1].split(':')[1]
            message = lista[1].split()
            sec = message[1].split('\r\n')[0]
            """
            User information is added to the dictionary
            """
            expires = datetime.now() + timedelta(seconds=int(sec))
            date = expires.strftime(TIME)
            client['Address'] = ADDRESS
            client['Expires'] = date
            """The client is drop out or not, depend if sec is 0 or !=0 """
            if int(sec) == 0:
                try:
                    del self.diccdata[user]
                    print('The user', user, 'in', ADDRESS, ' was deleted\n')
                except KeyError:
                    print('The user', user, 'not found\n')
            else:
                self.diccdata[user] = (client)
                print('The user', user, 'in', ADDRESS)
                print('Expires in', expires)

            del_lista = []
            now = datetime.now().strftime(TIME)
            for user in self.diccdata:
                if self.diccdata[user]['Expires'] <= now:
                    del_lista.append(user)
            for user in del_lista:
                del self.diccdata[user]
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            self.register2json()


if __name__ == "__main__":
    # Listens at localhost ('') port 1995
    # and calls the EchoHandler class to manage the request
    THEPORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', THEPORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
